import {AfterViewInit, Component, ElementRef, ViewChild} from '@angular/core';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import * as L from 'leaflet';

@Component({
    selector: 'app-tab1',
    templateUrl: 'tab1.page.html',
    styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements AfterViewInit {
    @ViewChild('map') mapContainer: ElementRef;
    map: any;

    constructor(private geolocation: Geolocation) {
        this.locationWatch();
    }

    ionViewDidEnter() {
        this.geolocation.getCurrentPosition().then((resp) => {
            let markerGroup = L.featureGroup();
            L.circle([resp.coords.latitude, resp.coords.longitude], {
                radius: 450
            }).addTo(this.map);
            let marker: any = L.marker([resp.coords.latitude, resp.coords.longitude]).on('click', () => {
                alert('Marker clicked');
            });
            markerGroup.addLayer(marker);
            this.centerLeafletMapOnMarker(this.map, marker);
            this.map.addLayer(markerGroup);
        }).catch((error) => {
            console.log('Error getting location', error);
        });
    }

    centerLeafletMapOnMarker(map, marker) {
        let latLngs = [marker.getLatLng()];
        let markerBounds = L.latLngBounds(latLngs);
        map.fitBounds(markerBounds);
    }

    loadmap() {
        this.map = L.map("map");
        L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
            {
                maxZoom: 15,
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'
            })
            .addTo(this.map);

        this.map.locate({
            setView: true,
            maxZoom: 10,

        }).on('locationfound', (e) => {

        }).on('locationerror', (err) => {
            alert(err.message);
        })
    }


    locationWatch() {
        let watch = this.geolocation.watchPosition();
        watch.subscribe((data) => {
            console.log(data);
            // data can be a set of coordinates, or an error (if an error occurred).
            // data.coords.latitude
            // data.coords.longitude
        });
    }

    ngAfterViewInit(): void {
        this.loadmap();
    }
}
